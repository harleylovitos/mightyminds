import { Container } from "@material-ui/core";
import "./App.scss";
import Body from "./components/Body";
import Header from "./components/Header";
import PageHeader from "./components/PageHeader";

function App() {
  return (
    <div>
      <Header />
      <PageHeader />
      <Container>
        <Body />
      </Container>
    </div>
  );
}

export default App;
