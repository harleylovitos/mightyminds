import React from "react";
import "./style.scss";
import { makeStyles } from "@material-ui/core/styles";
import { Add } from "@material-ui/icons";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { Button, Container } from "@material-ui/core";
const PageHeader = () => {
  const useStyles = makeStyles(() => ({
    pageHeader: {
      minHeight: "8rem",
      backgroundColor: "#0D2250",
      position: "relative",
      color: "#fff",
      paddingTop: "1rem",
      backgroundImage: "url(/images/books.png) ",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "0px 35px",
    },
    books: {
      marginLeft: "8rem",
    },
    info: {
      marginLeft: "4rem",
    },
    activity: {
      color: "#98A5BF",
      fontWeight: "600",
      fontSize: ".8rem",
    },
    activitySummary: {
      fontSize: ".8rem",
      marginTop: ".7rem",
    },
    activitySummaryDetails: {
      marginRight: "1.2rem",
      float: "left",
    },
    indicatorDue: {
      marginRight: ".5rem",
      backgroundColor: "#3972F5",
    },
    indicatorCompleted: {
      backgroundColor: "#5DC111",
    },
    indicatorOverdue: {
      backgroundColor: "#F14343",
    },
    buttons: {
      paddingTop: "2rem",
    },
    primaryButton: {
      color: "#fff",
      border: "1px solid #fff",
      marginRight: ".8rem",
    },
    containedButton: {
      backgroundColor: "#3972F5",
      color: "#fff",
    },
    arrowDown: {
      minWidth: "2rem",
      paddingLeft: "0",
      paddingRight: "0",
      marginLeft: ".2rem",
      backgroundColor: "#3972F5",
      color: "#fff",
    },
  }));
  const classes = useStyles();
  return (
    <div className={classes.pageHeader}>
      <Container>
        {/* <div className={classes.books + ` float-left`}>
          <img src="/images/books.png" alt="Books" />
        </div> */}
        <div className={classes.info + ` float-left`}>
          <h3>Welcom back, Jasmine</h3>
          <div className={classes.activity}>
            <label>WEEK 4 ACTIVITY SUMMARY</label>
          </div>
          <div className={classes.activitySummary}>
            <div className={classes.activitySummaryDetails}>
              <div className={classes.indicatorDue + ` indicator`}></div>
              <label>Due this week: 330</label>
            </div>
            <div className={classes.activitySummaryDetails}>
              <div className={classes.indicatorCompleted + ` indicator`}></div>
              <label>Completed: 240</label>
            </div>
            <div className={classes.activitySummaryDetails}>
              <div className={classes.indicatorOverdue + ` indicator`}></div>
              <label>Overdue: 33</label>
            </div>
          </div>
        </div>
        <div className={classes.buttons + ` float-right`}>
          <Button variant="outlined" className={classes.primaryButton}>
            My Calendar
          </Button>
          <Button variant="outlined" className={classes.primaryButton}>
            Weekly Rerpot
          </Button>
          <Button variant="contained" className={classes.containedButton}>
            <Add /> Assign Activity
          </Button>
          <Button variant="contained" className={classes.arrowDown}>
            <KeyboardArrowDownIcon />
          </Button>
        </div>
      </Container>
    </div>
  );
};

export default PageHeader;
