import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link, Button, Grid, Box, Typography, List, ListItemText } from "@material-ui/core";
import { Add, Help, ArrowForward } from "@material-ui/icons";
import ClassesBox from "../ClassesBox";

const Body = () => {
  const useStyles = makeStyles(() => ({
    viewAllClasses: {
      display: "inline-block",
      float: "right",
    },
    subHeader: {
      paddingBottom: "1.8rem",
    },
    widgetWorks: {
      width: "85%",
      backgroundColor: "#D1DFFF",
      borderRadius: ".3rem",
      marginTop: "6.7rem",
      padding: "1.5rem",
      textAlign: "center",
      "& h5": {
        fontSize: "1.2rem",
        marginBottom: ".8rem",
        textAlign: "left",
      },
      "& span": {
        color: "#747C88",
        textAlign: "left",
        display: "block",
      },
      "& button": {
        backgroundColor: "#3872F5",
        width: "100%",
        textAlign: "center",
        padding: ".6rem",
      },
      "& button span": {
        color: "#fff",
        textAlign: "center",
      },
      "& a": {
        textDecoration: "underline",
        fontSize: ".9rem",
      },
    },
    sidebarImage: {
      textAlign: "center",
      position: "relative",
      top: "-51px",
    },
    rightSidebar: {},
    helpSupport: {
      width: "100%",
      borderRadius: ".3rem",
      marginTop: "1.3rem",
      border: "2px solid #F0F1F5",
      backgroundColor: "#fff",
      paddingBottom: "0",
      "& .links,& .title": {
        display: "flex",
        alignItems: "center",
        margin: "0 1rem",
      },
      "& .links a": {
        color: "gray",
      },
      "& .title": {
        borderBottom: "1px solid #F0F1F5",
        padding: "1rem 0",
      },
      "& .title svg": {
        color: "#A9B4CA",
        marginRight: ".5rem",
      },
      "& .MuiListItemText-root,& .other-links .MuiLink-root": {
        padding: ".3rem 0",
        color: "gray",
      },
      "& .other-links": {
        borderTop: "1px solid #F0F1F5",
      },
      "& .links .MuiLink-root,& .other-links .MuiLink-root": {
        verticalAlign: "super",
      },
      "& .other-links .MuiListItemText-root": {
        padding: ".2rem 1rem",
      },
    },
    addNewClass: {
      border: ".2rem dashed #C2C7D1",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100%",
      "& svg": {
        fontSize: "3rem",
        color: "#C2C7D1",
      },
    },
  }));
  const classes = useStyles();
  return (
    <Grid container spacing={2}>
      <Grid item md={9} xs={12}>
        <div className={classes.subHeader}>
          <h3>Here ware your classes</h3>
          <label>
            Select a class to view this week&#39;s assigned activities and begin your lesson.
          </label>
          <a href="" className={classes.viewAllClasses}>
            View all classes
          </a>
        </div>
        <Grid container spacing={4}>
          {[
            {
              title: "12ENGA",
              color: "#6BC62A",
              years: 12,
              subject: "English",
              activitiesCount: 3,
            },
            {
              title: "12ENGB",
              color: "#487DF6",
              years: 12,
              subject: "English",
              activitiesCount: 0,
            },
            {
              title: "08MATHS",
              color: "#F7AD33",
              years: 8,
              subject: "Maths",
              activitiesCount: 3,
            },
            {
              title: "09SCI",
              color: "#EF408B",
              years: 9,
              subject: "Science",
              activitiesCount: 6,
            },
            {
              title: "09HASS",
              color: "#6A44D2",
              years: 9,
              subject: "Humanities and Social Studiest",
              activitiesCount: 1,
            },
          ].map((data, key) => {
            return (
              <Grid key={key} item md={4} xs={12}>
                <ClassesBox
                  activitiesCount={data.activitiesCount}
                  subject={data.subject}
                  years={data.years}
                  title={data.title}
                  color={data.color}
                />
              </Grid>
            );
          })}
          <Grid item md={4} xs={12}>
            <Box className={classes.addNewClass}>
              <Add />
            </Box>
          </Grid>
        </Grid>
      </Grid>
      <Grid className={classes.rightSidebar} item md={3} xs={12}>
        <Box className={classes.widgetWorks}>
          <div className={classes.sidebarImage}>
            <img src="/images/working.png" />
          </div>
          <Typography variant="h5">Explore your new portal</Typography>
          <Typography variant="p">
            Improved class focus, unit plans, live lessons, additional tracking features and much
            more.
          </Typography>
          <br />
          <div>
            <Button className="" variant="contained">
              See how it works
            </Button>
          </div>
          <br />
          <Link size="small">Getting started guide</Link>
        </Box>
        <Box className={classes.helpSupport}>
          <div className="title">
            <Help />
            <Typography variant="h5">Help & Support</Typography>
          </div>
          <div className="links">
            <List>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Visit help centre</Link>
              </ListItemText>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Send us your feedback</Link>
              </ListItemText>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Make a request suggestion</Link>
              </ListItemText>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Report an issue</Link>
              </ListItemText>
            </List>
          </div>
          <div className="other-links">
            <List>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Teacher support group</Link>
              </ListItemText>
              <ListItemText href="#simple-list">
                <ArrowForward />
                <Link>Schedule a consultation</Link>
              </ListItemText>
            </List>
          </div>
        </Box>
      </Grid>
    </Grid>
  );
};

export default Body;
