/* eslint-disable react/prop-types */
import React from "react";
import {
  Box,
  List,
  ListItemText,
  CardContent,
  Typography,
  CardActions,
  Link,
} from "@material-ui/core";
import { FlashOn, Star } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

const ClassesBox = ({ title, color, years, subject, activitiesCount }) => {
  // eslint-disable-next-line react/prop-types
  const useStyles = makeStyles(() => ({
    box: {
      padding: "1rem",
      borderRadius: ".3rem",
      border: "1px solid #E0E3E9",
      borderTop: `4px solid ` + color,
      color: "#747C88",
      backgroundColor: "#fff",
    },
    header: {
      display: "flex",
      alignItems: "center",
    },
    subHeader: {
      display: "flex",
      alignItems: "center",
      fontSize: ".9rem",
      "& span": {
        display: "block",
        float: "left",
        padding: "0 .5rem",
        width: "20%",
        borderLeft: "1px solid #ccc",
      },
      "& span:first-child": {
        width: "10%",
        borderLeft: "none",
      },
      "& span:last-child": {
        width: "50%",
      },
    },
    action: {
      borderTop: "1px solid #E0E3E9",
    },
    addStudent: {
      color: "#adadad",
      cursor: "pointer",
    },
    badgeCount: {
      backgroundColor: "#EBEEF2",
      borderRadius: ".3rem",
      display: "inline-block",
      float: "right",
      padding: ".1rem 1rem",
      fontWeight: "600",
    },
    cardContent: {
      padding: "0",
    },
    boxIcon: {
      borderRadius: ".2rem",
      backgroundColor: color,
      padding: ".2rem",
      width: "1.2rem",
      height: "1.2rem",
      marginRight: ".4rem",
    },
    flashIcon: {
      color: color,
      backgroundColor: "#fff",
      borderRadius: "5rem",
    },
    starIcon: {
      color: "#3B73F5",
    },
    subject: {
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",
    },
  }));
  const classes = useStyles();
  return (
    <div className={classes.box}>
      <React.Fragment>
        <CardContent className={classes.cardContent}>
          <Box className={classes.header} mb={1}>
            <Box className={classes.boxIcon}>
              <FlashOn className={classes.flashIcon} fontSize="small" />
            </Box>
            <Typography variant="h5" sx={{ fontSize: 14 }} color="text.secondary">
              {title}
            </Typography>
          </Box>
          <div className={classes.subHeader}>
            <span>
              <Star fontSize="small" className={classes.starIcon} />
            </span>
            <span>Year {years}</span>
            <span className={classes.subject}>{subject}</span>
          </div>
          <Box mt={1}>
            <List>
              <ListItemText href="#simple-list">
                <label>Activities due this week </label>
                <label className={classes.badgeCount}>{activitiesCount}</label>
              </ListItemText>
              <ListItemText href="#simple-list">Assign activities</ListItemText>
              <ListItemText href="#simple-list">Class calender</ListItemText>
            </List>
          </Box>
        </CardContent>
        <CardActions className={classes.action}>
          <Link size="small" className={classes.addStudent}>
            + Add Student
          </Link>
        </CardActions>
      </React.Fragment>
    </div>
  );
};

export default ClassesBox;
