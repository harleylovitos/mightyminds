import React from "react";
import "./style.scss";
import { Help } from "@material-ui/icons/";
import { makeStyles } from "@material-ui/core/styles";
const Header = () => {
  const useStyles = makeStyles(() => ({
    helpIcon: {
      verticalAlign: "middle",
    },
  }));
  const classes = useStyles();

  return (
    <header className="header">
      <div className="logo">
        <img src="/images/logo.png" className="App-logo" alt="logo" />
      </div>
      <div className="main-menu-container">
        <ul className="main-menu">
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>Classes</a>
          </li>
          <li>
            <a>Planner</a>
          </li>
          <li>
            <a>School Data</a>
          </li>
          <li>
            <a>Library</a>
          </li>
        </ul>
      </div>

      <div className="account float-right">
        <div className="details float-left">
          <label className="name">Jasmine Finn</label>
          <label className="type">Teacher Account</label>
        </div>
        <div className="icon float-left">
          <label>JF</label>
        </div>
      </div>

      <div className="help float-right">
        <a>
          <Help className={classes.helpIcon} />
          Help Center
        </a>
      </div>
      <div className="clearfix"></div>
    </header>
  );
};

export default Header;
